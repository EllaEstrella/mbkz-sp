package zcu.fav.mbkz_sp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * The main activity started on app launch.
 * @author Eliska Mourycova
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startGameActivity(View v){
        Intent i = new Intent(this, GameActivity.class);
        startActivity(i);
    }

    public void startAboutActivity(View v){
        Intent i = new Intent(this, AboutActivity.class);
        startActivity(i);
    }
    public void startSettingsActivity(View v){
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    // kill the app completely
    public void quitApplication(View v){
        finishAndRemoveTask();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // to make the window big and pretty
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}

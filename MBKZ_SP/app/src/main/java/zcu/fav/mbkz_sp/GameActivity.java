package zcu.fav.mbkz_sp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NavUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Random;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

/**
 * This class handles the logic of the game.
 * @author Eliska Mourycova
 */
public class GameActivity extends AppCompatActivity {

    // PUBLIC FIELDS
    /**
     * List of all the dropped cards by both players
     * - static so that the Deck class can access this without needing the instance of this class
     */
    public static ArrayList<Card> droppedCards;

    /**
     * List of the Cards in the players hand
     */
    public ArrayList<Card> cardsInHandPlayer;
    /**
     * List of the Cards in the opponents hand
     */
    public ArrayList<Card> cardsInHandOpponent;

    /**
     * List of the selectors image views shown when a dame was played
     */
    public ArrayList<ImageView> colorSelectors;
    /**
     * List of the selectors drawables shown when a dame was played
     */
    public ArrayList<Drawable> colorSelectorsDrawables;
    /**
     * Image view representing the selected color
     */
    public ImageView currColorSelected;

    /**
     * The SoundPool class to handle the sound effects
     */
    public SoundPool sounds;
    /**
     * The negative sound
     */
    public int beepNegative;
    /**
     * Sound on card drop
     */
    public int beepCardDrop;
    //public int victorySFX;
    //public int failSFX;

    /**
     * Counter keeping track of how many sevens were played
     */
    public int numOf7nsPlayed;

    /**
     * Flag saying if player is on an ace streak
     */
    public boolean playerAceStreak = false;
    /**
     * Flag saying if opponent is on an ace streak
     */
    public boolean oppAceStreak = false;

    // PRIVATE GLOBAL VARIABLES
    /**
     * The instance of the CardsListener class
     */
    private CardsListener cardsListener;
    /**
     * Instance of a deck
     */
    private Deck deck;

    /**
     * The width and height of a card in players hand
     */
    private int cardWidth, cardHeight;
    /**
     * The width and height of a card in opponents hand
     */
    private int cardWidthOpp, cardHeightOpp;

    /**
     * The circular spinner
     */
    private ProgressBar pb;
    /**
     * Text view saying opponent is thinking
     */
    private TextView oppPlayingTV;
    /**
     * The horizontal scroll view
     */
    private HorizontalScrollView hsv;

    /**
     * The card on top of dropped cards
     */
    private LinearLayout topCard;
    /**
     * Opponents layout for cards in hand
     */
    private LinearLayout oppCardsLayout;
    /**
     * Players layout for cards in hand
     */
    private LinearLayout playerCardsLayout;

    /**
     * Thext shown on game over
     */
    private TextView youWinText;

    /**
     * The main layout containg everything
     */
    private ConstraintLayout mainLayout;
    /**
     * Button for returning to main menu
     */
    private Button returnToMMBt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);


        // setting the card metrics - calculated according to screen size:
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int screenHeight = displayMetrics.heightPixels;
        int screenWidth = displayMetrics.widthPixels;

        cardWidth = screenWidth / 5;
        cardHeight = screenHeight / 6;

        cardWidthOpp = screenWidth / 8;
        cardHeightOpp = screenHeight / 9;


        // creating instances of game logic componentns:
        cardsListener = new CardsListener(this);
        cardsInHandPlayer = new ArrayList<>(4);
        cardsInHandOpponent = new ArrayList<>(4);
        deck = new Deck();
        droppedCards = new ArrayList<>();
        colorSelectors = new ArrayList<>(4);
        colorSelectors.add((ImageView) findViewById(R.id.imageViewSelectAcorns));
        colorSelectors.add((ImageView) findViewById(R.id.imageViewSelectBalls));
        colorSelectors.add((ImageView) findViewById(R.id.imageViewSelectHearts));
        colorSelectors.add((ImageView) findViewById(R.id.imageViewSelectLeaves));
        for(int i = 0; i < colorSelectors.size(); i++){
            colorSelectors.get(i).setOnTouchListener(cardsListener);
        }
        colorSelectorsDrawables = new ArrayList<>(4);
        colorSelectorsDrawables.add(getDrawable(getId("acorns_color", R.drawable.class)));
        colorSelectorsDrawables.add(getDrawable(getId("balls_color", R.drawable.class)));
        colorSelectorsDrawables.add(getDrawable(getId("hearts_color", R.drawable.class)));
        colorSelectorsDrawables.add(getDrawable(getId("leaves_color", R.drawable.class)));

        currColorSelected = (ImageView) findViewById(R.id.imageViewCurrColor);

        // assigning verschiedene veci:
        pb = (ProgressBar) findViewById(R.id.progressBar);
        oppPlayingTV = findViewById(R.id.textViewOppThinking);
        pb.setVisibility(View.INVISIBLE);
        oppPlayingTV.setVisibility(View.INVISIBLE);
        hsv = findViewById(R.id.horizontalScrollView);
        oppCardsLayout = findViewById(R.id.opponentCardsLayout);
        playerCardsLayout = findViewById(R.id.handCardsLayout);
        youWinText = findViewById(R.id.textViewYouWin);
        mainLayout = findViewById(R.id.mainLayout);//(ConstraintLayout) getWindow().getDecorView().getRootView();
        returnToMMBt = findViewById(R.id.buttonToMM);

        ImageView backIV = new ImageView(getApplicationContext());
        Drawable bg = getDrawable(getId("back", R.drawable.class));
        backIV.setTag("back");
        backIV.setBackground(bg);
        //context = getApplicationContext();



        // building the sounds
        AudioAttributes audioAttributes = new AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributes.USAGE_GAME)
                .build();

        sounds = new SoundPool.Builder()
                .setMaxStreams(4)
                .setAudioAttributes(audioAttributes)
                .build();

        beepNegative = sounds.load(getApplicationContext(), R.raw.beep_negative, 1);
        beepCardDrop = sounds.load(getApplicationContext(), R.raw.beep_card_drop, 1);
        //victorySFX = sounds.load(getApplicationContext(), R.raw.victory_sfx, 1);
        //failSFX = sounds.load(getApplicationContext(), R.raw.fail_sfx, 1);




        // setting listeners:
        findViewById(R.id.topCardLayout).setOnDragListener(cardsListener);
        findViewById(R.id.handCardsLayout).setOnDragListener(cardsListener);
        findViewById(R.id.imageViewDeck).setOnTouchListener(cardsListener);

//        ImageView deckCardIV = findViewById(R.id.imageViewDeck);
//        deckCardIV.setOnTouchListener(cardsListener);

        // initializing game:
        createInitCards();
        positionCards();

        // adding a card to the dropped cards
        topCard = (LinearLayout) findViewById(R.id.topCardLayout);


        // adding one card to the dropped ones
        ImageView iv;
        Card c;
        iv = new ImageView(getApplicationContext());
        c = deck.getFromDeck();
        Drawable bg2 = getDrawable(getId(c.getImgName(), R.drawable.class));
        iv.setTag(c.getImgName());
        iv.setBackground(bg2);
        c.setImageView(iv);
        droppedCards.add(c);
        topCard.addView(iv);


        // no sevens played so far
        numOf7nsPlayed = 0;


        // decide who goes first
        TextView whoFirst = findViewById(R.id.textViewWhoFirst);
        whoFirst.setVisibility(View.VISIBLE);
        whoFirst.setTextColor(Color.GRAY);

        if((new Random()).nextDouble() > 0.5){
            // opp goes first
            whoFirst.setText("Opponent starts");
            whoFirst.startAnimation(AnimationUtils.loadAnimation(youWinText.getContext(), R.anim.fade_out));
            opponentPlay();
        }
        else{
            whoFirst.setText("You start");
            whoFirst.startAnimation(AnimationUtils.loadAnimation(youWinText.getContext(), R.anim.fade_out));
        }

    }

    /**
     * Creates initial cards to both the players and the opponents hand
     */
    private void createInitCards() {
        Card c;
        for(int i = 0; i < 4; i++){
            c = createOneCardToHand(false);
            cardsInHandPlayer.add(c);

            c = createOneCardToHand(true);
            cardsInHandOpponent.add(c);
        }
    }

    /**
     * Gets one card from the dock to either the players or the opponents hand
     * @param opp   Card for the opponent?
     * @return      A card from the deck
     */
    private Card createOneCardToHand(boolean opp){
        ImageView iv;
        Card c;

        iv = new ImageView(getApplicationContext());

        // getting from the deck
        c = deck.getFromDeck();
        Drawable bg = getDrawable(getId(c.getImgName(), R.drawable.class));
        iv.setTag(c.getImgName());
        iv.setBackground(bg);


        c.setImageView(iv);

        // setting the listener only for the players cards
        if(!opp){
            iv.setOnTouchListener(cardsListener);
        }

        // return the card
        return c;
    }

    /**
     * Puts either one or many (bc of sevens) cards to the players hand
     */
    public void newCardToHand(){
        if(numOf7nsPlayed != 0){
            // if sevens were played, put two times more cards in the hand
            for(int i = 0; i < numOf7nsPlayed * 2; i++){
                Card c = createOneCardToHand(false);
                cardsInHandPlayer.add(c);
            }

            // reset the counter
            numOf7nsPlayed = 0;
        }
        else {
            // sle only one card
            Card c = createOneCardToHand(false);
            cardsInHandPlayer.add(c);
        }

        // position the cards
        positionCards();


    }

    /**
     * Gets the id for the specified resource and a specified class
     * @param resourceName  The name of the resource
     * @param c             The class to search in
     * @return              The id
     */
    private int getId(String resourceName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resourceName);
            return idField.getInt(idField);
        } catch (Exception e) {
            throw new RuntimeException("No resource ID found for: "
                    + resourceName + " / " + c, e);
        }
    }

    /**
     * Positions the cards in both the players and the opponents hand
     */
    public void positionCards(){

        // PLAYER
        LinearLayout.LayoutParams lp;
        lp = new LinearLayout.LayoutParams(cardWidth, cardHeight);

        playerCardsLayout.removeAllViews();
        // decide if the count of the cards is even or odd
        boolean even = cardsInHandPlayer.size() % 2 == 0;

        for(int i = 0; i < cardsInHandPlayer.size(); i++) {
            ImageView iv = cardsInHandPlayer.get(i).getImageView();

            // the tilt
            if(i < cardsInHandPlayer.size() / 2){
                iv.setRotation(-10);
            }
            else if(i == (cardsInHandPlayer.size() / 2) && !even){
                iv.setRotation(0);
            }
            else {
                iv.setRotation(10);
            }

            // set the Y shift
            iv.setY(cardHeight / 2f);

            iv.setLayoutParams(lp);
            // add it to the layout
            playerCardsLayout.addView(iv);
        }

        // OPPONENT
        lp = new LinearLayout.LayoutParams(cardWidthOpp, cardHeightOpp);

        oppCardsLayout.removeAllViews();
        even = cardsInHandOpponent.size() % 2 == 0;

        for(int i = 0; i < cardsInHandOpponent.size(); i++) {
            ImageView iv = new ImageView(getApplicationContext());

            Drawable bg = getDrawable(getId("back", R.drawable.class));
            iv.setBackground(bg);


            // the tilt - opposite direction
            if(i < cardsInHandOpponent.size() / 2){
                iv.setRotation(10);
            }
            else if(i == (cardsInHandOpponent.size() / 2) && !even){
                iv.setRotation(0);
            }
            else {
                iv.setRotation(-10);
            }

            // set the Y shift
            iv.setY(cardHeightOpp / 5f);

            iv.setLayoutParams(lp);
            // add it to the layout
            oppCardsLayout.addView(iv);
        }
    }


    /**
     * Primitive AI for mimicking an opponents move
     */
    public void opponentPlay() {

        // if the player played an ace, do nothing and let player play again
        if(playerAceStreak){
            return;
        }

        // the time to pretend the opponent is deciding
        int maxTimeWait;// = 2000;
        int minTimeWait;//= 1000;

        // finding out if the opponent can play a card
        boolean canPlay = false;
        for(int i = 0; i < cardsInHandOpponent.size(); i++){
            if(checkMove(false, cardsInHandOpponent.get(i).getImageView())){
                canPlay = true;
                break;
            }
        }
        if(canPlay){
            maxTimeWait = 1300;
            minTimeWait = 900;
        }
        else{
            // if opponent cant play any card - needs more time to think
            maxTimeWait = 2000;
            minTimeWait = 1500;

        }

        Random rand = new Random();
        // randomly decide the actual time of deciding
        int randTime = rand.nextInt((maxTimeWait - minTimeWait) + 1) + minTimeWait;

        // show the spinner and the text
        pb.setVisibility(View.VISIBLE);
        oppPlayingTV.setVisibility(View.VISIBLE);

        if(cardsInHandOpponent.size() == 1){ // if he only has one card, then there's not much to think about
            randTime = 500;
        }

        // disable the window
        setWindowEnabled(false, false);

        // create new handler to do stuff after some time
        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // hide the spinner and the text
                pb.setVisibility(View.INVISIBLE);
                oppPlayingTV.setVisibility(View.INVISIBLE);


                // the actual move:
                int indToRemove = -1;
                int indToRemovePreferable = -1;
                for(int i = 0; i < cardsInHandOpponent.size(); i++){
                    if(checkMove(false, cardsInHandOpponent.get(i).getImageView())){
                        // if opponent encounters a card which they can dicard:
                        indToRemove = i;
                        //break;
                    }
                    if(cardsInHandOpponent.get(i).getValue() == 7){ // playing a 7 is preferable
                        if(checkMove(false, cardsInHandOpponent.get(i).getImageView())){
                            indToRemovePreferable = i;
                            //break;
                        }
                    }
                }

                if(indToRemovePreferable != -1){
                    indToRemove = indToRemovePreferable;
                }

                if (indToRemove != -1){
                    // set selected color invisible
                    currColorSelected.setVisibility(View.INVISIBLE);

                    LinearLayout.LayoutParams lpNormalOpp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    // drop the card
                    droppedCards.add(cardsInHandOpponent.get(indToRemove));
                    topCard.removeAllViews();
                    oppCardsLayout.removeView(cardsInHandOpponent.get(indToRemove).getImageView());
                    //ImageView iv = new ImageView(context);

                    cardsInHandOpponent.get(indToRemove).getImageView().setLayoutParams(lpNormalOpp);
                    cardsInHandOpponent.get(indToRemove).getImageView().setRotation(0);
                    cardsInHandOpponent.get(indToRemove).getImageView().setY(0);

                    topCard.addView(cardsInHandOpponent.get(indToRemove).getImageView());

                    oppAceStreak = false;
                    if(cardsInHandOpponent.get(indToRemove).getValue() == 14){ // playing the dame
                        // look at the first card in his hand and select that color
                        int color;
                        if(indToRemove != 0){
                            color = cardsInHandOpponent.get(0).getColor();
                        }
                        else{
                            color = cardsInHandOpponent.get(cardsInHandOpponent.size() - 1).getColor();
                        }


                        switch (color){
                            case 0:
                                currColorSelected.setBackground(colorSelectorsDrawables.get(2));
                                droppedCards.get(GameActivity.droppedCards.size() - 1).setColor(0);
                                break;
                            case 1:
                                currColorSelected.setBackground(colorSelectorsDrawables.get(3));
                                droppedCards.get(GameActivity.droppedCards.size() - 1).setColor(1);
                                break;
                            case 2:
                                currColorSelected.setBackground(colorSelectorsDrawables.get(1));
                                droppedCards.get(GameActivity.droppedCards.size() - 1).setColor(2);
                                break;
                            case 3:
                                currColorSelected.setBackground(colorSelectorsDrawables.get(0));
                                droppedCards.get(GameActivity.droppedCards.size() - 1).setColor(3);
                                break;
                        }

                        currColorSelected.setVisibility(View.VISIBLE);
                    }
                    else if(cardsInHandOpponent.get(indToRemove).getValue() == 7){
                        numOf7nsPlayed++;
                    }
                    else if(cardsInHandOpponent.get(indToRemove).getValue() == 11){
                        oppAceStreak = true;
                    }

                    // remove from hand
                    cardsInHandOpponent.remove(indToRemove);

                    // play card drop sound
                    if(SettingsActivity.SFXOn)
                        sounds.play(beepCardDrop, 0.9f, 0.9f, 1, 0, 1);

                    // position the cards
                    positionCards();

                    // play again if the last one was an ace and not empty hand
                    if(cardsInHandOpponent.size() != 0 && oppAceStreak){
                        opponentPlay();
                    }
                    if(cardsInHandOpponent.size() == 0){ // check if opp wins
                        onOpponentWins();
                    }




                }
                else {
                    // draw new cards
                    if(numOf7nsPlayed != 0){
                        for(int i = 0; i < numOf7nsPlayed * 2; i++){
                            Card c = createOneCardToHand(true);
                            cardsInHandOpponent.add(c);
                        }
                        numOf7nsPlayed = 0;
                    }
                    else {
                        Card c = createOneCardToHand(true);
                        cardsInHandOpponent.add(c);
                    }


                    positionCards();
                }

                // enable the window again if the opp hasnt won
                if(cardsInHandOpponent.size() != 0)
                    setWindowEnabled(true, false);


            }
        }, randTime);



    }

    /**
     * Actions to perform if the player wins
     */
    public void onPlayerWins(){
        youWinText.setVisibility(View.VISIBLE);
        youWinText.setText("You Win!");
        youWinText.startAnimation(AnimationUtils.loadAnimation(youWinText.getContext(), R.anim.shake_win));

        //disableWindow();
        setWindowEnabled(false, true);

//        if(SettingsActivity.SFXOn)
//            sounds.play(victorySFX, 0.9f, 0.9f, 1, 0, 1);


        KonfettiView viewKonfetti = findViewById(R.id.viewKonfetti);
        viewKonfetti.build()
                .addColors(Color.YELLOW, Color.BLUE, Color.MAGENTA)
                .setDirection(0.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(2000L)
                .addShapes(Shape.CIRCLE, Shape.RECT)
                .addSizes(new Size(12, 5))
                .setPosition(-50f, viewKonfetti.getWidth() + 50f, -50f, -50f)
                .streamFor(300, 5000L);
    }

    /**
     * Actions to perform if the opponent wins
     */
    private void onOpponentWins(){
        youWinText.setVisibility(View.VISIBLE);
        youWinText.setText("You Lose");
        youWinText.setTextColor(Color.BLUE);
        //disableWindow();
        setWindowEnabled(false, true);

//        if(SettingsActivity.SFXOn)
//            sounds.play(failSFX, 0.9f, 0.9f, 1, 0, 1);
    }

    /**
     * Renders the window enabled/disabled
     * @param enabled   Should the controls be enabled?
     * @param gameOver  Is the game over?
     */
    public void setWindowEnabled(boolean enabled, boolean gameOver){
        for(int i = 0; i < mainLayout.getChildCount(); i++){
            //mainLayout.getChildAt(i).setBackgroundColor(Color.GRAY);
            if(mainLayout.getChildAt(i).getId() != R.id.imageViewSelectLeaves
            && mainLayout.getChildAt(i).getId() != R.id.imageViewSelectHearts
            && mainLayout.getChildAt(i).getId() != R.id.imageViewSelectBalls
            && mainLayout.getChildAt(i).getId() != R.id.imageViewSelectAcorns){
                mainLayout.getChildAt(i).setEnabled(enabled);
            }

        }

        for(int i = 0; i < playerCardsLayout.getChildCount(); i++){
            playerCardsLayout.getChildAt(i).setEnabled(enabled);
        }


        if(gameOver){
            // if the game is over, show additional controls and hide some redundant ones
            returnToMMBt.setVisibility(View.VISIBLE);
            returnToMMBt.setEnabled(true);
            findViewById(R.id.buttonSurrender).setVisibility(View.INVISIBLE);
            findViewById(R.id.buttonPlayAgain).setVisibility(View.VISIBLE);
            findViewById(R.id.buttonPlayAgain).setEnabled(true);
        }

    }

    /**
     * Restart the whole activity
     * @param v A view
     */
    public void playAgain(View v){
        startActivity(getIntent());
        finish();
        overridePendingTransition(0, 0);
    }

    /**
     * Return to main menu
     * @param v
     */
    public void returnToMainMenu(View v){
        NavUtils.navigateUpFromSameTask(this);
    }


    /**
     * Check if move is valid
     * @param fromDeck  Is someone trying to take a new card from the deck?
     * @param v         Which view (card) is being played?
     * @return          True if valid move, false otherwise
     */
    public boolean checkMove(boolean fromDeck, View v){

        if(fromDeck){
            boolean deckOk = true;
            int firstOkCardInd = -1;
            for(int i = 0; i < cardsInHandPlayer.size(); i++){
                if(isCardPlayable(cardsInHandPlayer.get(i).getImageView())){
                    if(firstOkCardInd == -1){
                        firstOkCardInd = i;
                        int x, y;
                        x = cardsInHandPlayer.get(i).getImageView().getLeft();
                        y = cardsInHandPlayer.get(i).getImageView().getTop();
                        hsv.scrollTo(x,y); // scrolling to the position of the first card that fits
                    }
                    cardsInHandPlayer.get(i).getImageView().startAnimation(AnimationUtils.loadAnimation(cardsInHandPlayer.get(i).getImageView().getContext(), R.anim.shake));
                    deckOk = false;


                }
            }

            return deckOk;
        }
        else {
            return isCardPlayable(v);
        }

    }

    /**
     * Check if a card is playable
     * @param v The view representing a card
     * @return  True if playable, false otherwise
     */
    public boolean isCardPlayable(View v){
        Card cardBeingPlayed = new Card((String) v.getTag());
        Card currTopCard = droppedCards.get(droppedCards.size() - 1);

        if((cardBeingPlayed.getColor() == currTopCard.getColor() ||
                cardBeingPlayed.getValue() == currTopCard.getValue())
        || (cardBeingPlayed.getValue() == 14 && (currTopCard.getValue() != 7 || numOf7nsPlayed == 0))
        ){

            if(currTopCard.getValue() == 7 && cardBeingPlayed.getValue() != 7 && numOf7nsPlayed > 0){
                return false;
            }
            return true;
        }
        return false;

    }

    /**
     * What to do if the player wants to surrender
     * @param v A view
     */
    public void surrenderClick(View v){

        final Activity ga = this;
        final Context c1 = v.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(c1); // show an alert
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                NavUtils.navigateUpFromSameTask(ga);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                return;
            }
        });

        builder.setMessage("Are you sure you want to surrender and quit the game? No progress will be saved.")
                .setTitle("Surrender")
                .setIcon(R.drawable.alert);
        AlertDialog dialog = builder.create();
        dialog.show();

    }




    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // to make the window big and pretty
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}

package zcu.fav.mbkz_sp;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.view.View;

/**
 * This class represents a custom DragShadowBuilder - it extends this class too
 * @author Eliska Mourycova
 */
public class CustomDragShadowBuilder extends View.DragShadowBuilder {
    // PRIVATE VARIABLES
    /**
     * The shadow view
     */
    private View v;
    /**
     * The scale factor for the original view
     */
    private Point scale;

    public CustomDragShadowBuilder(View v) {
        super(v);
        this.v = v;
    }

    @Override
    public void onDrawShadow(Canvas canvas) {


        if(CardsListener.cardPlayable){
            v.getBackground().setColorFilter(0x3003ff79, PorterDuff.Mode.SRC_ATOP); // green filter if the card is playable
        }
        else {
            v.getBackground().setColorFilter(0x30eb4034, PorterDuff.Mode.SRC_ATOP); // red filter if the card is not playable
        }

        // scale according to the scale factor
        canvas.scale(scale.x / (float)getView().getWidth(), scale.y / (float)getView().getHeight());
        getView().draw(canvas);
    }
    @Override
    public void onProvideShadowMetrics(Point shadowSize, Point touchPoint) {
        int width;
        int height;

        // create new width and height for the shadow
        width = (int)(getView().getWidth() * 1.5f);
        height = (int)(getView().getHeight() * 1.5f);
        shadowSize.set(width, height);

        scale = shadowSize;

        // where should the touch point be? - bottom of the card in the middle
        touchPoint.set(width/2, height);

    }

}

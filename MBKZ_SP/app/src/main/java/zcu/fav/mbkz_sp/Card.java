package zcu.fav.mbkz_sp;

import android.widget.ImageView;

/**
 * This class represents a playing card.
 * @author ELiska Mourycova
 */
public class Card {
    // PRIVATE VARIABLES
    /**
     * The color (symbol of a card)
     */
    private int color;
    /**
     * The value of a card
     */
    private int value;
    /**
     * The image view coresponding to this card
     */
    private ImageView imageView;

    /**
     * Creates new card from a color and a value
     * @param color The color
     * @param value The value
     */
    public Card(int color, int value) {
        this.color = color;
        this.value = value;
    }

    /**
     * Creates new card from a string
     * @param nameTag   The string tag of the card
     */
    public Card(String nameTag) {
        if(nameTag.contains("hearts")){
            color = 0;
        }
        else if(nameTag.contains("leaves")){
            color = 1;
        }
        else if(nameTag.contains("balls")){
            color = 2;
        }
        else if(nameTag.contains("acorns")){
            color = 3;
        }

        if(nameTag.contains("7")){
            value = 7;
        }
        else if(nameTag.contains("8")){
            value = 8;
        }
        else if(nameTag.contains("9")){
            value = 9;
        }
        else if(nameTag.contains("10")){
            value = 10;
        }
        else if(nameTag.contains("ace")){
            value = 11;
        }
        else if(nameTag.contains("king")){
            value = 12;
        }
        else if(nameTag.contains("down")){
            value = 13;
        }
        else if(nameTag.contains("up")){
            value = 14;
        }
    }

    /**
     * Gets the name of an image corresponding to the card
     * @return The name of an image corresponding to the card
     */
    public String getImgName() {

        String retString = "col_";
        switch (color)
        {
            case 0:
                retString += "hearts_";
                break;
            case 1:
                retString += "leaves_";
                break;
            case 2:
                retString += "balls_";
                break;
            case 3:
                retString += "acorns_";
                break;
        }
        retString += "val_";
        switch (value)
        {
            case 7:
                retString += "7";
                break;
            case 8:
                retString += "8";
                break;
            case 9:
                retString += "9";
                break;
            case 10:
                retString += "10";
                break;
            case 11:
                retString += "ace";
                break;
            case 12:
                retString += "king";
                break;
            case 13:
                retString += "down";
                break;
            case 14:
                retString += "up";
                break;
        }

        return retString;

    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {

        this.imageView = imageView;
    }
}

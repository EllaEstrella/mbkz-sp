package zcu.fav.mbkz_sp;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This class represents a deck of cards
 * @author Eliska Mourycova
 */
public class Deck {
    // PRIVATE VARIABLES
    /**
     * List of cards in the deck
     */
    private ArrayList<Card> cardsInDeck;

    /**
     * Creates new deck of 32 cards as if it was a real deck
     */
    public Deck(){

        // create new deck
        cardsInDeck = new ArrayList<>(32);

        // fill it with 32 cards
        int minColor = 0;
        int maxColor = 3;

        int minValue = 7;
        int maxValue = 14;

        for(int i = minColor; i <= maxColor; i++){
            for(int j = minValue; j <= maxValue; j++){
                cardsInDeck.add(new Card(i, j));
            }
        }
        // now we have the 32 cards in the deck

        // now shuffle it (unnecessary but accurate)
        Collections.shuffle(cardsInDeck);
    }

    /**
     * Gets the top card from the deck
     * @return The top card from the deck
     */
    public Card getFromDeck(){

        // refill the deck from the dropped cards if it is empty
        if(cardsInDeck.isEmpty()){
            refillDeck();
        }
        // get the top card from the deck and remove it
        Card card = cardsInDeck.get(cardsInDeck.size() - 1);
        cardsInDeck.remove(cardsInDeck.size() - 1);

        // return the card
        return card;
    }

    /**
     * Refills the deck by shuffling the dropped cards
     */
    private void refillDeck(){

        // add all the dropped cards to the deck but leave the last one dropped
        cardsInDeck.addAll(GameActivity.droppedCards);
        Card cardToLeave = GameActivity.droppedCards.get(GameActivity.droppedCards.size() - 1);
        GameActivity.droppedCards.clear();

        GameActivity.droppedCards.add(cardToLeave);
        cardsInDeck.remove(cardToLeave);

        // shuffle it
        Collections.shuffle(cardsInDeck);
    }

}

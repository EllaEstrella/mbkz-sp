package zcu.fav.mbkz_sp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

/**
 * The settings activity, takes care only of the SFX on/off switch.
 * @author Eliska Mourycova
 */
public class SettingsActivity extends AppCompatActivity {

    // PUBLIC FIELDS
    /**
     * A flag saying whether the user wishes to have the SFX on.
     */
    public static boolean SFXOn = true; // initialization here important

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // find the switch
        Switch swich = (Switch) findViewById(R.id.switchSFX);

        // get the saved state of the switch
        SharedPreferences settings = getSharedPreferences("zcu.fav.mbkz_sp", 0);
        boolean checked = settings.getBoolean("switchSFX", true);
        swich.setChecked(checked);
        SFXOn = swich.isChecked();

        // save the state on change
        swich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences settings = getSharedPreferences("zcu.fav.mbkz_sp", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("switchSFX", isChecked);
                SFXOn = isChecked; // save it to the flag
                editor.apply(); // apply the changes
            }
        });

    }
}

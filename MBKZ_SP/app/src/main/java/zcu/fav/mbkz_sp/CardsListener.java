package zcu.fav.mbkz_sp;

import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * This class represents a custom Listener used for cards touching and dragging.
 * @author Eliska Mourycova
 */
public class CardsListener implements View.OnTouchListener, View.OnDragListener {

    // PUBLIC FIELDS
    /**
     * A flag saying whether the card being touched or dragged is playable.
     */
    public static boolean cardPlayable = false;

    // PRIVATE GLOBAL VARIABLES
    /**
     * A shadow builder creating a shadow when a card is being dragged
     */
    private View.DragShadowBuilder shadowBuilder;

    /**
     * Instance of the GameActivity class
     */
    private GameActivity gameActivity;

    /**
     * The constructor of this class
     * @param ga The instance of the GameActivity class
     */
    public CardsListener(GameActivity ga) {
        this.gameActivity = ga;
    }

    /**
     * A method invoked when the user touches a deck
     * @param v The view representing the deck
     */
    private void onTouchingDeck(View v){
        ImageView view = (ImageView) v;
        // just play the animation
        view.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.fade_in));
        view.invalidate();
    }

    /**
     * The method invoked when user starts to drag a view
     * @param v The dragged view
     */
    private void startDragging(View v){
        // instantiating the shadow builder
        shadowBuilder = new CustomDragShadowBuilder(v);

        // setting alpha to the old view - when set invisible, it causes problems
        v.setAlpha(0.7f);
        //v.setVisibility(View.INVISIBLE);

        // start the actual dragging with opaque shadow
        v.startDragAndDrop(null, shadowBuilder, v, View.DRAG_FLAG_OPAQUE);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {

        // if the finger just touched the screen:
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            if(v.getId() == R.id.imageViewDeck){ // if user touched the deck
                // check if it is allowed to draw new card:
                boolean deckOk = gameActivity.checkMove(true, null);

                if(!deckOk){ // it is illegal to get new card from the deck (playable cards in hand)

                    // play the no no sound
                    if(SettingsActivity.SFXOn)
                        gameActivity.sounds.play (gameActivity.beepNegative, 0.9f, 0.9f, 1, 0, 1);

                    // return handled
                    return true;
                }
                // else perform actions associated to the deck
                onTouchingDeck(v);
                // player has no ace streak now
                gameActivity.playerAceStreak = false;
                // assign new card to the player's hand
                gameActivity.newCardToHand();
                // let the opponent play
                gameActivity.opponentPlay();

                //GameActivity.sounds.play (GameActivity.beepCardDrop, 0.9f, 0.9f, 1, 0, 1);

                return false;
            }
            else if(v.getId() == R.id.imageViewSelectAcorns ||
                    v.getId() == R.id.imageViewSelectBalls ||
                    v.getId() == R.id.imageViewSelectHearts ||
                    v.getId() == R.id.imageViewSelectLeaves){ // if user touched one of the selecotrs after playing a dame

                //onTouchingDeck(v);


                switch (v.getId()){ // decide which color they chose and perform actions
                    case R.id.imageViewSelectAcorns:
                        gameActivity.currColorSelected.setBackground(gameActivity.colorSelectorsDrawables.get(0));
                        GameActivity.droppedCards.get(GameActivity.droppedCards.size() - 1).setColor(3);
                        break;
                    case R.id.imageViewSelectBalls:
                        gameActivity.currColorSelected.setBackground(gameActivity.colorSelectorsDrawables.get(1));
                        GameActivity.droppedCards.get(GameActivity.droppedCards.size() - 1).setColor(2);
                        break;
                    case R.id.imageViewSelectHearts:
                        gameActivity.currColorSelected.setBackground(gameActivity.colorSelectorsDrawables.get(2));
                        GameActivity.droppedCards.get(GameActivity.droppedCards.size() - 1).setColor(0);
                        break;
                    case R.id.imageViewSelectLeaves:
                        gameActivity.currColorSelected.setBackground(gameActivity.colorSelectorsDrawables.get(3));
                        GameActivity.droppedCards.get(GameActivity.droppedCards.size() - 1).setColor(1);
                        break;
                }

                // show the selected color
                gameActivity.currColorSelected.setVisibility(View.VISIBLE);

                // hide the slectors
                setColorPickersVisibility(false);

                // set the whole screen to disabled
                gameActivity.setWindowEnabled(true, false);

                // let the opponent play if the player has any cards in their hand and they're not on an ace streak
                if(gameActivity.cardsInHandPlayer.size() != 0 && !gameActivity.playerAceStreak){
                    gameActivity.opponentPlay();
                }

                // return event handled
                return true;

            }

            // decide if the card being touched is playable
            cardPlayable = gameActivity.isCardPlayable(v);

            // then start dragging
            startDragging(v);

            // return event handled
            return true;
        }
        else {
            // return event not handled
            return false;
        }
    }


    /**
     * This method performs game logic on card drop
     * @param view  The view representing the card which was dropped
     */
    private void gameLogicOnDrop(View view) {
        //GameActivity ga = (GameActivity) gameAct.get();

        // modifying the collections:
        int indToRemove = -1;
        for(int i = 0; i < gameActivity.cardsInHandPlayer.size(); i++){
            // find the card in the player's hand to be removed
            if(view.getTag() == gameActivity.cardsInHandPlayer.get(i).getImageView().getTag()){
                indToRemove = i;
            }
        }

        // if we found it, then remove it
        if(indToRemove != -1){
            gameActivity.cardsInHandPlayer.remove(indToRemove);
        }

        //final boolean clicked = false;

        // create a new card from the card dropped
        Card c = new Card((String)view.getTag());

        // add it to dropped cards
        gameActivity.droppedCards.add(c);

        // position the cards in hands
        gameActivity.positionCards();

        // check if the player won
        if(gameActivity.cardsInHandPlayer.size() == 0){
            // they won - do stuff
            gameActivity.onPlayerWins();

            // stop all else
            return;
        }

        // setting ace streak to false
        gameActivity.playerAceStreak = false;


        if(c.getValue() == 14){
            // if they played the changer, select the color
            setColorPickersVisibility(true);
            // disable the window
            gameActivity.setWindowEnabled(false, false);

        }
        else if(c.getValue() == 7){
            // they played a seven
            gameActivity.numOf7nsPlayed++;
        }
        else if(c.getValue() == 11){
            //gameActivity.numOfAcesPlayed ++;
            // they played an ace - theyre on a streak now
            gameActivity.playerAceStreak = true;
        }
    }

    /**
     * Sets the color selectors visibility (after playing a dame)
     * @param visible Visible or invisible?
     */
    private void setColorPickersVisibility(boolean visible){
        if(visible){
            for(int i = 0; i < gameActivity.colorSelectors.size(); i++){
                gameActivity.colorSelectors.get(i).setVisibility(View.VISIBLE);
            }
        }
        else {
            for(int i = 0; i < gameActivity.colorSelectors.size(); i++){
                gameActivity.colorSelectors.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }



    @Override
    public boolean onDrag(View v, DragEvent event) {
        // get the drag action
        int action = event.getAction();

        // create 'normal' layout params
        LinearLayout.LayoutParams lpNormal;
        lpNormal = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        // get view from the shadow builder
        ImageView iv = (ImageView) shadowBuilder.getView();

        // now decide what to do according to the action
        switch (action) {
            case DragEvent.ACTION_DRAG_LOCATION:
                break;
            case DragEvent.ACTION_DRAG_STARTED:
                //View view = (View) event.getLocalState();
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                break;
            case DragEvent.ACTION_DROP: // drop is important

                View view = (View) event.getLocalState(); // get the local state view
                LinearLayout container = (LinearLayout) v; // get the container of the view

                // if the card is NOT playable
                if(!gameActivity.isCardPlayable(view)){
                    // AND they dragged it over the dropped cards
                    if(container.getId() == R.id.topCardLayout){
                        // play no no sound
                        if(SettingsActivity.SFXOn)
                            gameActivity.sounds.play(gameActivity.beepNegative, 0.9f, 0.9f, 1, 0, 1);
                    }

                    // quit performing anything else
                    break;
                }

                // set a color selected from the slectors invisible, but only if they dropped it to dropped cards
                if(container.getId() == R.id.topCardLayout)
                    gameActivity.currColorSelected.setVisibility(View.INVISIBLE);

                // remove the color filter
                iv.getBackground().clearColorFilter();

                // again we're looking at a situation of dropping a card to dropped cards
                if(container.getId() == R.id.topCardLayout){
                    // get the current owner of the view
                    ViewGroup owner = (ViewGroup) view.getParent();
                    owner.removeView(view); // and remove the view from its old parent

                    // set normal parameters, rotation and Y shift
                    view.setLayoutParams(lpNormal);
                    view.setRotation(0);
                    view.setY(0);

                    // remove all views from the container (new parent) and remove listeners from it
                    container.removeAllViews();
                    view.setOnTouchListener(null);
                    view.setOnDragListener(null);
                    container.addView(view); // add view to new parent

                    // card was dropped correctly - play a sound
                    if(SettingsActivity.SFXOn)
                        gameActivity.sounds.play(gameActivity.beepCardDrop, 0.9f, 0.9f, 1, 0, 1);

                    // perform the logic
                    gameLogicOnDrop(view);

                    // let the opponent play if it is a reasonable thing to do
                    if(gameActivity.colorSelectors.get(0).getVisibility() == View.INVISIBLE
                    && gameActivity.cardsInHandPlayer.size() != 0){
                        gameActivity.opponentPlay();
                    }
                }

                // set the alpha back to opaque
                view.setAlpha(1f);
                view.setVisibility(View.VISIBLE);
                break;
            case DragEvent.ACTION_DRAG_ENDED: // the drag ended
                // just make the card look normal again
                iv.getBackground().clearColorFilter();
                view = (View) event.getLocalState();
                view.setAlpha(1f);
                view.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }

        return true;
    }

























//    private Point getTouchPositionFromDragEvent(View item, DragEvent event) {
//        Rect rItem = new Rect();
//        item.getGlobalVisibleRect(rItem);
//        return new Point(rItem.left + Math.round(event.getX()), rItem.top + Math.round(event.getY()));
//    }
//
//    private boolean isTouchInsideOfView(View view, Point touchPosition) {
//        Rect rScroll = new Rect();
//        view.getGlobalVisibleRect(rScroll);
//        return isTouchInsideOfRect(touchPosition, rScroll);
//    }
//
//    private boolean isTouchInsideOfRect(Point touchPosition, Rect rScroll) {
//        return touchPosition.x > rScroll.left && touchPosition.x < rScroll.right //within x axis / width
//                && touchPosition.y > rScroll.top && touchPosition.y < rScroll.bottom; //withing y axis / height
//    }




}